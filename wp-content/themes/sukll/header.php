<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package sukll
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<?php wp_head(); ?>
    <style type="text/css">
	/*! CSS Used from: /wp-content/plugins/bwp-minify/min/?f=wp-content/plugins/contact-form-7/includes/css/styles.css,wp-content/plugins/wordpress-seo/css/dist/adminbar-702.min.css,wp-content/themes/sukll/style.css,wp-content/plugins/recent-posts-widget-with-thumbnails/public.css,wp-content/plugins/duplicate-post/duplicate-post.css,wp-content/plugins/js_composer/assets/css/js_composer.min.css */
header,nav{display:block;}
a{background-color:transparent;}
a:active,a:hover{outline:0;}
img{border:0;}
p{margin-bottom:0.5em;}
*,*:before,*:after{box-sizing:inherit;}
ul{margin:0
0 1.5em 3em;}
ul{list-style:disc;}
li>ul{margin-bottom:0;margin-left:1.5em;}
img{height:auto;max-width:100%;}
a{color:royalblue;}
a:hover,a:focus,a:active{color:midnightblue;}
a:focus{outline:thin dotted;}
a:hover,a:active{outline:0;}
.site-header{display:flex;align-items:flex-end;}
.main-navigation{clear:both;display:block;float:left;width:100%;}
.main-navigation .menu-primary-anglais-container{float:right;}
.main-navigation
ul{display:none;list-style:none;margin:0;padding-left:0;}
.main-navigation ul
ul{box-shadow:0 3px 3px rgba(0, 0, 0, 0.2);float:left;position:absolute;left:-999em;z-index:99999;background:#608eb5;padding:20px
120px 20px 20px;margin-top:20px;}
.main-navigation ul ul
a{color:#fff;}
.main-navigation ul li:hover > ul{left:inherit;}
.main-navigation ul li:hover > ul > li{text-transform:uppercase;float:none;flex-grow:1;width:100%;}
.main-navigation
li{float:left;}
.main-navigation
a{font-size:20px;font-family:Montserrat;display:block;text-decoration:none;color:#333;}
@media screen and (min-width:37.5em){
.main-navigation
ul{display:block;}
}
.site-header:before,.site-header:after{content:"";display:table;table-layout:fixed;}
.site-header:after{clear:both;}
.custom-logo-link{display:inline-block;}
.site-branding{padding-bottom:20px;}
@media (max-width:425px){
.main-navigation a{font-size:calc(18vw/5);}
.main-navigation ul li:hover > ul > li:nth-child(1),.main-navigation .sub-menu a{font-size:calc(17vw/5);}
}
@media (min-width:426px) and (max-width:767px){
.main-navigation a{font-size:calc(20vw/7);}
.main-navigation ul li:hover > ul > li:nth-child(1),.main-navigation .sub-menu a{font-size:calc(18vw/7);}
}
@media (max-width:1024px){
#top-menu{display:none;}
}
@media (max-width:1366px) and (min-width:768px){
.main-navigation a{font-size:calc(20vw/15);}
.main-navigation ul li:hover > ul > li:nth-child(1),.main-navigation .sub-menu a{font-size:calc(18vw/15);}
.site-header{padding:20px
calc(190vw/20) 0px;}
.main-navigation div>ul>li{padding:0px
calc(10vw/13) 0px;}
.main-navigation div #primary-menu>li{padding:0px
calc(10vw/13) 20px;}
.custom-logo-link{width:calc(223vw/13);}
}
@media (min-width:1367px){
.main-navigation a{font-size:calc(20vw/20);}
.main-navigation ul li:hover > ul > li:nth-child(1),.main-navigation .sub-menu a{font-size:calc(18vw/20);}
.site-header{padding:20px
calc(287vw/20) 0px;}
.main-navigation div>ul>li{padding:5px
10px 0px;}
.main-navigation div #primary-menu>li{padding:5px
10px 20px;}
}
.main-navigation .sub-menu
li{margin-bottom:15px;}
.admin-bar
.topHeader{top:35px;}
.topHeader a[href*="facebook"]{font-size:0px!important;font-family:FontAwesome;font-weight:normal;line-height:0.5;margin-left:25px;}
.topHeader a[href*="facebook"]:before{font-size:25px;content:"\f09a";}
.topHeader a[href*="linkedin"]{font-size:0px!important;font-family:FontAwesome;font-weight:normal;line-height:0.5;}
.topHeader a[href*="linkedin"]:before{font-size:25px;content:"\f0e1";}
#socialmedia-menu{float:right;margin-bottom:15px;}
#socialmedia-menu li
a{color:#608eb5;}
#top-menu{float:right;width:100%;text-align:right;}
#top-menu
li{float:none;}
#top-menu li
a{color:#608eb5;font-size:16px;font-weight:600;text-transform:uppercase;}
.menu-item-language-current a,.menu-item-language a:hover{color:#608eb5;}
.main-navigation ul>.menu-item-language~.menu-item-language{padding-left:0!important;}
.main-navigation ul > .menu-item-language ~ .menu-item-language a::before{content:" ";width:2px;border-left:2px solid #333;padding-right:10px;}
@media (max-width:767px){
#masthead{display:none!important;}
}
/*! CSS Used from: Embedded */
.site-title,.site-description{position:absolute;clip:rect(1px, 1px, 1px, 1px);}
/*! CSS Used fontfaces */
@font-face{font-family:FontAwesome;src:url(/wp-content/plugins/js_composer/assets/lib/bower/font-awesome/fonts/fontawesome-webfont.eot?v=4.7.0);src:url(/wp-content/plugins/js_composer/assets/lib/bower/font-awesome/fonts/fontawesome-webfont.eot#iefix&v=4.7.0) format('embedded-opentype'),url(/wp-content/plugins/js_composer/assets/lib/bower/font-awesome/fonts/fontawesome-webfont.woff2?v=4.7.0) format('woff2'),url(/wp-content/plugins/js_composer/assets/lib/bower/font-awesome/fonts/fontawesome-webfont.woff?v=4.7.0) format('woff'),url(/wp-content/plugins/js_composer/assets/lib/bower/font-awesome/fonts/fontawesome-webfont.ttf?v=4.7.0) format('truetype'),url(/wp-content/plugins/js_composer/assets/lib/bower/font-awesome/fonts/fontawesome-webfont.svg?v=4.7.0#fontawesomeregular) format('svg');font-weight:400;font-style:normal;}

header,nav{display:block;}
a{background-color:transparent;}
a:active,a:hover{outline:0;}
img{border:0;}
*,*:before,*:after{box-sizing:inherit;}
ul{margin:0
0 1.5em 3em;}
ul{list-style:disc;}
li>ul{margin-bottom:0;margin-left:1.5em;}
img{height:auto;max-width:100%;}
a:hover,a:focus,a:active{color:midnightblue;}
a:focus{outline:thin dotted;}
a:hover,a:active{outline:0;}
.custom-logo-link{display:inline-block;}
@media (max-width:1366px) and (min-width:768px){
.custom-logo-link{width:calc(223vw/13);}
}
.menu-item-language-current a,.menu-item-language a:hover{color:#608eb5;}
.headerMobile #cssmenu,.headerMobile #cssmenu ul,.headerMobile #cssmenu ul li,.headerMobile #cssmenu ul li a,.headerMobile #cssmenu #head-mobile{border:0;list-style:none;line-height:1;display:block;position:relative;box-sizing:border-box;}
.headerMobile #cssmenu:after,.headerMobile #cssmenu>ul:after{content:".";display:block;clear:both;visibility:hidden;line-height:0;height:0;}
.headerMobile
.logo{position:relative;z-index:123;padding:10px;font:18px verdana;color:#6ddb07;float:left;width:15%;}
.headerMobile .logo
a{color:#6ddb07;}
.headerMobile
nav{position:relative;width:980px;margin:0
auto;}
.headerMobile
#cssmenu{font-family:sans-serif;background:#fff;}
.headerMobile #cssmenu>ul{margin:0px!important;}
.headerMobile #cssmenu
ul{padding:0px;}
.headerMobile #cssmenu ul
ul{position:absolute;left:-9999px;}
.headerMobile #cssmenu ul ul
li{height:0;background:#fff;transition:all .25s ease;}
.headerMobile #cssmenu ul ul li
a{padding:17px;font-size:12px;letter-spacing:1px;text-decoration:none;color:#333;font-weight:700;}
.headerMobile #cssmenu ul ul li a:hover{color:#fff;}
.headerMobile #cssmenu ul ul li:hover>a{color:#fff;}
.headerMobile #cssmenu ul ul li:last-child>a{border-bottom:0;}
.headerMobile #cssmenu #head-mobile{display:none;}
.headerMobile #cssmenu>ul>li{float:left;}
.headerMobile #cssmenu>ul>li>a{padding:17px;font-size:12px;letter-spacing:1px;text-decoration:none;color:#333;font-weight:700;}
.headerMobile #cssmenu>ul>li:hover{background:#608eb5!important;transition:background .3s ease;}
.headerMobile #cssmenu>ul>li:hover>a{color:#fff;}
.headerMobile #cssmenu>ul>li.has-sub>a{padding-right:30px;}
.headerMobile #cssmenu>ul>li.has-sub>a:after{position:absolute;top:22px;right:11px;width:8px;height:2px;display:block;background:#333;content:'';}
.headerMobile #cssmenu>ul>li.has-sub>a:before{position:absolute;top:19px;right:14px;display:block;width:2px;height:8px;background:#333;content:'';transition:all .25s ease;}
.headerMobile #cssmenu>ul>li.has-sub:hover>a:before{top:23px;height:0;}
.headerMobile #cssmenu li:hover>ul{left:auto;}
.headerMobile #cssmenu li:hover>ul>li{height:35px;}
@media screen and (max-width:767px){
.headerMobile
.logo{top:0;left:0;width:90%;height:46px;padding:20px
60px 0;float:none;}
.headerMobile
nav{width:100%;}
.headerMobile
#cssmenu{width:100%;}
.headerMobile #cssmenu
ul{width:100%;display:none;}
.headerMobile #cssmenu ul
li{width:100%;border-top:1px solid #444;}
.headerMobile #cssmenu ul li:hover{background:#363636;}
.headerMobile #cssmenu ul li
a{width:100%;border-bottom:0;}
.headerMobile #cssmenu ul
ul{position:relative;left:0;width:100%;margin:0;text-align:left;}
.headerMobile #cssmenu ul ul
li{height:auto;background:#fff!important;}
.headerMobile #cssmenu ul ul li
a{width:100%;border-bottom:0;padding-left:25px;color:#333;background:none;}
.headerMobile #cssmenu ul ul li:hover{background:#363636!important;}
.headerMobile #cssmenu ul ul li:hover>a{color:#fff;}
.headerMobile #cssmenu>ul>li{float:none;}
.headerMobile #cssmenu>ul>li.has-sub>a:after{display:none;}
.headerMobile #cssmenu>ul>li.has-sub>a:before{display:none;}
.headerMobile #cssmenu #head-mobile{display:block;padding:60px;color:#333;font-size:12px;font-weight:700;}
.headerMobile #cssmenu .submenu-button{position:absolute;z-index:99;right:0;top:0;display:block;border-left:1px solid #444;height:46px;width:46px;cursor:pointer;}
.headerMobile #cssmenu .submenu-button:after{position:absolute;top:22px;right:19px;width:8px;height:2px;display:block;background:#333;content:'';}
.headerMobile #cssmenu .submenu-button:before{position:absolute;top:19px;right:22px;display:block;width:2px;height:8px;background:#333;content:'';}
.headerMobile #cssmenu li:hover>ul>li{height:auto;}
.headerMobile
.button{width:55px;height:46px;position:absolute;right:10px;top:10px;cursor:pointer;z-index:12399994;}
.headerMobile .button:after{position:absolute;top:22px;right:20px;display:block;height:3px;width:20px;border-top:2px solid #333;border-bottom:2px solid #333;content:'';}
.headerMobile .button:before{transition:all .3s ease;position:absolute;top:16px;right:20px;display:block;height:3px;width:20px;background:#333;content:'';}
}
@media (min-width:768px){
.headerMobile{display:none;}
}
    </style>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'sukll' ); ?></a>

	<header id="masthead" class="site-header">

		<div class="site-branding">
			<?php
			the_custom_logo();
			if ( is_front_page() && is_home() ) : ?>
				<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
			<?php else : ?>
				<p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
			<?php
			endif;

			$description = get_bloginfo( 'description', 'display' );
			if ( $description || is_customize_preview() ) : ?>
				<p class="site-description"><?php echo $description; /* WPCS: xss ok. */ ?></p>
			<?php
			endif; ?>
		</div><!-- .site-branding -->
        <!--//TODO add menu-2 -->
		<nav id="site-navigation" class="main-navigation">
         <div class="topHeader padding-top-header">
                <?php
                wp_nav_menu( array(
                    'theme_location' => 'menu-3',
                    'menu_id'        => 'socialmedia-menu',
                    "container"=>null
                ) );
                ?>
                <?php
                wp_nav_menu( array(
                    'theme_location' => 'menu-2',
                    'menu_id'        => 'top-menu',
                    "container"=>null
                ) );
                ?>
            </div>
			<?php
				wp_nav_menu( array(
					'theme_location' => 'menu-1',
					'menu_id'        => 'primary-menu',
				) );
			?>
           
		</nav><!-- #site-navigation -->

	</header><!-- #masthead -->
    <header  class="headerMobile">
        <nav id='cssmenu'>
            <div class="logo">
                    <?php the_custom_logo();  ?></div>
            <div id="head-mobile"></div>
            <div class="button"></div>
            <?php
            wp_nav_menu( array(
                'theme_location' => 'menu-1',
                'menu_id'        => 'primary-menu',
                "container"=>null
            ) );
            ?>
        </nav>
    </header>
	<div id="content" class="site-content">
