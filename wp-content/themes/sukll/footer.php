<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package sukll
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer">
        <?php
		if(ICL_LANGUAGE_CODE =="fr"){
        	$post_12 = get_post(105);
		}else{
			$post_12 = get_post(830);
		}
        echo do_shortcode($post_12->post_content);

        ?>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
