<?php
/**
 * Created by PhpStorm.
 * User: mohamed
 * Date: 12/10/2017
 * Time: 14:22
 */
$vc_module->all_in_one("coverbanner-en",
    function ($atts, $content) {
        $atts = shortcode_atts([
            "cover" => "7",
			"cover2" => "7",
            "cssbg" => "false"
        ],
            $atts);
        $cover = explode(",", $atts["cover"]);
		$cover2 = explode(",", $atts["cover2"]);
        ob_start();
        ?>
        <style>
          

body .cover{
	z-index:0;
}
.banner img {vertical-align: middle;}

/* Slideshow container */
.slideshow-container {
  width:100%;
  position: relative;
  margin: auto;
}
.banner .maxh{
	max-height:800px;
}

/* Next & previous buttons */
.banner .prev, .banner .next {
  cursor: pointer;
  position: absolute;
  top: 50%;
  width: auto;
  padding: 16px;
  margin-top: -22px;
  color: white;
  font-weight: bold;
  font-size: 18px;
  transition: 0.6s ease;
  border-radius: 0 3px 3px 0;
}
/* Position the "next button" to the right */
.banner .next {
  right: 0;
  border-radius: 3px 0 0 3px;
}

/* On hover, add a black background color with a little bit see-through */
.banner .prev:hover, .banner .next:hover {
  background-color: rgba(0,0,0,0.8);
}

/* Caption text */
.banner .text {
  color: #727272;
  font-size: 0.79vw;
  padding: 8px 12px;
  position: relative;
  bottom: 8px;
  width: 50%;
  text-align: left;
  top: -27vw;
  left: 19.4vw;
  height:0;
}
.banner .social_bar{
	justify-content:flex-start;
	margin-bottom: 1vw;
}

/* The dots/bullets/indicators */
.banner .dot {
  height: 15px;
  width: 15px;
  margin: 0 2px;
  background-color: #bbb;
  border-radius: 50%;
  display: inline-block;
  transition: background-color 0.6s ease;
}

.banner .active {
  background-color: #717171;
}

/* Fading animation */
.banner .fade {
  -webkit-animation-name: fade;
  -webkit-animation-duration: 1.5s;
  animation-name: fade;
  animation-duration: 1.5s;
}

@-webkit-keyframes fade {
  from {opacity: .4} 
  to {opacity: 1}
}

@keyframes fade {
  from {opacity: .4} 
  to {opacity: 1}
}

/* On smaller screens, decrease text size */
@media only screen and (max-width: 300px) {
  .banner .prev, .banner .next, .banner .text{font-size: 11px}
}


        </style>
        <div class="cover" >

          
     
                <div class="banner slideshow-container">

<div class="mySlides fade maxh">
  <?= wp_get_attachment_image($cover[0], "full",false) ?>
  <div class="wpb_text_column wpb_content_element  vc_custom_1520499971918 titleCover">
		<div class="wpb_wrapper">
			<p class="title" style="text-align: center;"><span class="aaa">Welcome to the website of</span><br>
<strong> Johathan Boivin, Avocat Criminaliste</strong></p>

		</div>
	</div>
</div>

<div class="mySlides fade">
  <?= wp_get_attachment_image($atts['cover2'], "full",false) ?>
  <div class="wpb_text_column wpb_content_element  vc_custom_1520499971918 titleCover">
		<div class="wpb_wrapper">
			<p class="title" style="text-align: center;"><span class="aaa">text2</span><br>
<strong> 22222</strong></p>

		</div>
	</div>
</div>


<a class="prev" onclick="plusSlides(-1)">&#10094;</a>
<a class="next" onclick="plusSlides(1)">&#10095;</a>
</div>
  <div style="text-align:center">
  <span class="dot"></span> 
  <span class="dot"></span> 
</div>      
                
          
        </div>
        <?php
        return ob_get_clean();
    },
    "cover page banner - EN", [
        [
            "type" => "attach_images",
            "holder" => "div",
            "class" => "",
            "heading" => __("image cover", "my-text-domain"),
            "param_name" => "cover",
            "value" => __("", "my-text-domain")
        ],
		 [
            "type" => "attach_images",
            "holder" => "div",
            "class" => "",
            "heading" => __("image cover 2", "my-text-domain"),
            "param_name" => "cover2",
            "value" => __("", "my-text-domain")
        ]
    ]);