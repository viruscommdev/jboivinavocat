<?php
/**
 * Created by PhpStorm.
 * User: mohamed
 * Date: 12/10/2017
 * Time: 14:22
 */
$vc_module->all_in_one("coverbanner",
    function ($atts, $content) {
        $atts = shortcode_atts([
             "cover" => "7",
			 "covermobile" => "7",
			"line11" => "",
			"line12"=>"",
			"cover2" => "7",
			"cover2mobile" => "7",
			"line21" => "",
			"line22" =>"",
			"cover3" => "7",
			"cover3mobile" => "7",
			"line31" => "",
			"line32" =>"",
            "cssbg" => "false"
        ],
            $atts);
        $cover = explode(",", $atts["cover"]);
		$covermobile = explode(",", $atts["covermobile"]);
		$cover2 = explode(",", $atts["cover2"]);
		$cover2mobile = explode(",", $atts["cover2mobile"]);
        ob_start();
        ?>
        <style>
          

body .cover{
	z-index:0;
}
.mobile{
	display:none;
}
.banner img {vertical-align: middle;}

/* Slideshow container */
.slideshow-container {
  width:100%;
  position: relative;
  margin: auto;
}
.banner .maxh{
	max-height:800px;
}

/* Next & previous buttons */
.banner .prev, .banner .next {
  cursor: pointer;
  position: absolute;
  top: 50%;
  width: auto;
  padding: 16px;
  margin-top: -22px;
  color: white;
  font-weight: bold;
  font-size: 18px;
  transition: 0.6s ease;
  border-radius: 0 3px 3px 0;
}
/* Position the "next button" to the right */
.banner .next {
  right: 0;
  border-radius: 3px 0 0 3px;
}

/* On hover, add a black background color with a little bit see-through */
.banner .prev:hover, .banner .next:hover {
  background-color: rgba(0,0,0,0.8);
}

/* Caption text */
.banner .text {
  color: #727272;
  font-size: 0.79vw;
  padding: 8px 12px;
  position: relative;
  bottom: 8px;
  width: 50%;
  text-align: left;
  top: -27vw;
  left: 19.4vw;
  height:0;
}
.banner .social_bar{
	justify-content:flex-start;
	margin-bottom: 1vw;
}

/* The dots/bullets/indicators */
.banner .dot {
  height: 15px;
  width: 15px;
  margin: 0 2px;
  background-color: #bbb;
  border-radius: 50%;
  display: inline-block;
  transition: background-color 0.6s ease;
}

.banner .active {
  background-color: #717171;
}

/* Fading animation */
.banner .fade {
  -webkit-animation-name: fade;
  -webkit-animation-duration: 1.5s;
  animation-name: fade;
  animation-duration: 1.5s;
}

@-webkit-keyframes fade {
  from {opacity: .4} 
  to {opacity: 1}
}

@keyframes fade {
  from {opacity: .4} 
  to {opacity: 1}
}
@media only screen and (max-width: 767px) {
  .desktop{
	  display:none;
  }
  .mobile{
	  display:block;
  }
}

/* On smaller screens, decrease text size */
@media only screen and (max-width: 300px) {
  .banner .prev, .banner .next, .banner .text{font-size: 11px}
}


        </style>
        <div class="cover" >

          <div class="desktop">
     
                <div class="banner slideshow-container">

<div class="mySlides fade maxh">
  <?= wp_get_attachment_image($cover[0], "full",false) ?>
  <div class="wpb_text_column wpb_content_element  vc_custom_1520499971918 titleCover">
		<div class="wpb_wrapper">
			<p class="title" style="text-align: center;"><span class="aaa"> <?= $atts['line11'] ?></span><br>
<strong><?= $atts['line12'] ?></strong></p>

		</div>
	</div>
</div>

<div class="mySlides fade">
  <?= wp_get_attachment_image($atts['cover2'], "full",false) ?>
  <div class="wpb_text_column wpb_content_element  vc_custom_1520499971918 titleCover">
		<div class="wpb_wrapper">
			<p class="title" style="text-align: center;"><span class="aaa"><?= $atts['line21'] ?></span><br>
<strong> <?= $atts['line22'] ?></strong></p>

		</div>
	</div>
</div>
<div class="mySlides fade">
  <?= wp_get_attachment_image($atts['cover3'], "full",false) ?>
  <div class="wpb_text_column wpb_content_element  vc_custom_1520499971918 titleCover">
		<div class="wpb_wrapper">
			<p class="title" style="text-align: center;"><span class="aaa"><?= $atts['line31'] ?></span><br>
<strong> <?= $atts['line32'] ?></strong></p>

		</div>
	</div>
</div>

<a class="prev" onclick="plusSlides(-1)">&#10094;</a>
<a class="next" onclick="plusSlides(1)">&#10095;</a>
</div>
 				<div style="text-align:center">
  <span class="dot"></span> 
  <span class="dot"></span> 
</div>   

  		  </div>
          <div class="mobile">
      		    <div class="banner slideshow-container">

<div class="mySlidesmobile fade maxh">
  <?= wp_get_attachment_image($covermobile[0], "full",false) ?>
  <div class="wpb_text_column wpb_content_element  vc_custom_1520499971918 titleCover">
		<div class="wpb_wrapper">
			<p class="title" style="text-align: center;"><span class="aaa"> <?= $atts['line11'] ?></span><br>
<strong><?= $atts['line12'] ?></strong></p>

		</div>
	</div>
</div>

<div class="mySlidesmobile fade">
  <?= wp_get_attachment_image($atts['cover2mobile'], "full",false) ?>
  <div class="wpb_text_column wpb_content_element  vc_custom_1520499971918 titleCover">
		<div class="wpb_wrapper">
			<p class="title" style="text-align: center;"><span class="aaa"><?= $atts['line21'] ?></span><br>
<strong> <?= $atts['line22'] ?></strong></p>

		</div>
	</div>
</div>
<div class="mySlidesmobile fade">
  <?= wp_get_attachment_image($atts['cover3mobile'], "full",false) ?>
  <div class="wpb_text_column wpb_content_element  vc_custom_1520499971918 titleCover">
		<div class="wpb_wrapper">
			<p class="title" style="text-align: center;"><span class="aaa"><?= $atts['line31'] ?></span><br>
<strong> <?= $atts['line32'] ?></strong></p>

		</div>
	</div>
</div>


<a class="prev" onclick="plusSlidesmobile(-1)">&#10094;</a>
<a class="next" onclick="plusSlidesmobile(1)">&#10095;</a>
</div>
 				<div style="text-align:center">
  <span class="dotmobile"></span> 
  <span class="dotmobile"></span> 
</div> 
          </div>
                
          
        </div>
        <?php
        return ob_get_clean();
    },
    "cover page banner", [
        [
            "type" => "attach_images",
            "holder" => "div",
            "class" => "",
            "heading" => __("image cover", "my-text-domain"),
            "param_name" => "cover",
            "value" => __("", "my-text-domain")
        ],
		[
            "type" => "attach_images",
            "holder" => "div",
            "class" => "",
            "heading" => __("image cover mobile", "my-text-domain"),
            "param_name" => "covermobile",
            "value" => __("", "my-text-domain")
        ],
		 [
            "type" => "textfield",
            "holder" => "div",
            "class" => "",
            "heading" => __("line 1", "my-text-domain"),
            "param_name" => "line11",
            "value" => __("", "my-text-domain")
        ],
		[
            "type" => "textfield",
            "holder" => "div",
            "class" => "",
            "heading" => __("line 2", "my-text-domain"),
            "param_name" => "line12",
            "value" => __("", "my-text-domain")
        ],
		 [
            "type" => "attach_images",
            "holder" => "div",
            "class" => "",
            "heading" => __("image cover 2", "my-text-domain"),
            "param_name" => "cover2",
            "value" => __("", "my-text-domain")
        ],
		[
            "type" => "attach_images",
            "holder" => "div",
            "class" => "",
            "heading" => __("image cover 2 mobile", "my-text-domain"),
            "param_name" => "cover2mobile",
            "value" => __("", "my-text-domain")
        ],
		 [
            "type" => "textfield",
            "holder" => "div",
            "class" => "",
            "heading" => __("line 1", "my-text-domain"),
            "param_name" => "line21",
            "value" => __("", "my-text-domain")
        ],
		[
            "type" => "textfield",
            "holder" => "div",
            "class" => "",
            "heading" => __("line 2", "my-text-domain"),
            "param_name" => "line22",
            "value" => __("", "my-text-domain")
        ],
		[
            "type" => "attach_images",
            "holder" => "div",
            "class" => "",
            "heading" => __("image cover 3", "my-text-domain"),
            "param_name" => "cover3",
            "value" => __("", "my-text-domain")
        ],
		[
            "type" => "attach_images",
            "holder" => "div",
            "class" => "",
            "heading" => __("image cover 3 mobile", "my-text-domain"),
            "param_name" => "cover3mobile",
            "value" => __("", "my-text-domain")
        ],
		 [
            "type" => "textfield",
            "holder" => "div",
            "class" => "",
            "heading" => __("line 1", "my-text-domain"),
            "param_name" => "line31",
            "value" => __("", "my-text-domain")
        ],
		[
            "type" => "textfield",
            "holder" => "div",
            "class" => "",
            "heading" => __("line 2", "my-text-domain"),
            "param_name" => "line32",
            "value" => __("", "my-text-domain")
        ]
    ]);