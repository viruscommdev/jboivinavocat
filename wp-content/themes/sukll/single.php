<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package sukll
 */

get_header(); ?>
<div class="bgris" style="width:auto;"></div>
<div class="vc_row paddingContent" >

    

        <div id="primary" class="content-area">
            <main id="main" class="site-main">

                <?php
                while ( have_posts() ) : the_post();

                    get_template_part( 'template-parts/content', get_post_type() );

                    // If comments are open or we have at least one comment, load up the comment template.
                   
                endwhile; // End of the loop.
                ?>

            </main><!-- #main -->
        </div><!-- #primary -->

 
</div>
<?php

get_footer();
