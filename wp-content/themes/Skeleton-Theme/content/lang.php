<?php
/**
 * Created by PhpStorm.
 * User: mohamed
 * Date: 13/11/2017
 * Time: 09:48
 */
$langTxt=[];
if(ICL_LANGUAGE_CODE =="fr"){
    $langTxt['Obtenir l’itinéraire']="Obtenir l’itinéraire";
    $langTxt['Lundi']="Lundi";
    $langTxt['merc']="merc";
    $langTxt['Jeudi']="Jeudi";
    $langTxt['Vendredi']="Vendredi";
    $langTxt['1 samedi sur']="1 samedi sur";
    $langTxt['au']="au";
    $langTxt['suivez nous sur']="suivez nous sur";
    $langTxt['recherche']="recherche";
    $langTxt['Categories']="Catégories";
    $langTxt['CONSEILS LES PLUS RÉCENTES']="CONSEILS LES PLUS RÉCENTS";
    $langTxt['linkHome']="/";
    $langTxt['post-contact-id']=576;
    $langTxt['Partager']="Partager";
    $langTxt['Publié']="Publié";
    $langTxt['Lire']="Lire +";
    $langTxt['Lire nos conseils']="Lire nos conseils";
    $langTxt['addressLaval']="380 Boulevard Dagenais E,<br>Laval, QC H7M 5H4";
    $langTxt['addressMascouche']="2400 Av de l'Esplanade #101,<br>Mascouche, QC J7K 0T4";
}else{
    $langTxt['Obtenir l’itinéraire']="Get Directions";
    $langTxt['Lundi']="Monday";
    $langTxt['merc']="wednesday";
    $langTxt['Jeudi']="Thursday";
    $langTxt['Vendredi']="Friday";
    $langTxt['1 samedi sur']="1 Saturday on";
    $langTxt['au']="to";
    $langTxt['suivez nous sur']="follow us";
    $langTxt['recherche']="search";
    $langTxt['Categories']="Categories";
    $langTxt['CONSEILS LES PLUS RÉCENTES']="MOST RECENT ADVICE";
    $langTxt['linkHome']="/en";
    $langTxt['post-contact-id']=5359;
    $langTxt['Publié']="Published";
    $langTxt['Partager']="Share";
    $langTxt['Lire']="Read more";
    $langTxt['Lire nos conseils']="Read our tips";
    $langTxt['addressLaval']="380 Dagenais Boulevard Est,<br>Laval (QC) H7M 5H4";
    $langTxt['addressMascouche']="2400 de l’Esplanade Avenue<br>Mascouche (QC) J7K 0T4";
}
global $langTxt;