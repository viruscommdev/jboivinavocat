<?php
get_header();
?>
<div class="indexPage">
    <?php
    the_post();
    the_content();
    ?>
</div>
<?php get_footer(); ?>
