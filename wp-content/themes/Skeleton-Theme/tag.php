<?php
get_header();
?>
<div class="indexPage">
    <?php
    ob_start();
    ?>
    [vc_row][vc_column]
    [vsc_cover cover="221" covermobile="1195" covermobiletext="1138" height="23.5vw"]
    [/vc_column][/vc_row]
    [vc_row el_class="content20vw "]
    [vc_column width="8/12"]
    [vsc_articles tag="<?=$wp_query->get_queried_object_id()?>"]
    [/vc_column][vc_column width="4/12"]
    [vsc_sidebar]
    [/vc_column][/vc_row]
    <?php
    $str =ob_get_clean();
    echo do_shortcode($str);
    ?>
</div>
<?php get_footer(); ?>
