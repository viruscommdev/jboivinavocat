<?php
get_header();
function getExcerpt($str, $startPos = 0, $maxLength = 100)
{
    if (strlen($str) > $maxLength) {
        $excerpt = substr($str, $startPos, $maxLength - 3);
        $lastSpace = strrpos($excerpt, ' ');
        $excerpt = substr($excerpt, 0, $lastSpace);
        $excerpt .= '...';
    } else {
        $excerpt = $str;
    }

    return $excerpt;
}
function removeShortCode($content){
    $re = '/(?<=\[)([^]]+)(?=])/';

    preg_match_all($re, $content, $matches, PREG_SET_ORDER, 0);
    foreach($matches as $match){
        $content=str_replace("[".$match[1]."]","",$content);
    }
    return $content;
}
$search_term = (isset($_GET['s'])) ? $_GET['s'] : 0;
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

$args = [
    "s" => $search_term,
    "post_status" => "publish",
    'posts_per_page' => 6,
    'paged' => $paged
];
query_posts($args);
?>
<style>
    @media (min-width: 426px) {
        .cover {
            height: 23.5vw !important;
        }
    }
</style>
<div class="cover">

    <div class="desktop">
        <img width="1997" height="481" src="/wp-content/uploads/2017/10/Api_siteweb_Nos-Conseils_01.jpg"
             class="attachment-full size-full" alt="Conseils API"></div>
    <div class="mobile">

        <img width="320" height="623" src="/wp-content/uploads/2017/10/header_zone-Nos-Conseils.jpg"
             class="attachment-full size-full" alt="">
        <div class="logo_cover_bar"
             style="background-image: url('/wp-content/themes/Skeleton-Theme/img/x_mobile_02.png')"></div>
    </div>
</div>
<div class="searchPage content20vw ">
    <div class="sidePost ">

        <div class="blogPage">
            <h2 class="font24 mfont32 fontbold">
                Articles
            </h2>
            <div class="list">
                <?php if (!have_posts()) {
                    ?>
                    there's no posts
                    <?php
                } ?>
                <?php while (have_posts()) {
                    the_post();
                    ?>
                    <div class="article <?= get_post_type() ?>">
                        <div class="img">
                            <?php
                            if (get_post_thumbnail_id() == "") {
                                $re = '/(?<=cover=")([0-9]+)(?=")/';
                                preg_match_all($re, get_the_content(), $matches, PREG_SET_ORDER, 0);

                                if (isset($matches[0][1]))
                                    $idImg = $matches[0][1];
                                else
                                    $idImg = "2998";
                                echo wp_get_attachment_image($idImg, "custom-med-461");
                            } else
                                echo wp_get_attachment_image(get_post_thumbnail_id(), "full");
                            ?>
                            <div class="text">
                                <?= get_the_title() ?>
                            </div>
                        </div>
                        <div class="except">
                            <span class="date"><?= get_the_date('d/m/Y') ?></span>
                            <?php
                            if (get_the_excerpt() == "...") {
                                echo getExcerpt(wp_strip_all_tags(removeShortCode(get_the_content())),0,150);
                            } else {
                                echo get_the_excerpt();
                            }
                            ?>
                        </div>
                        <div class="btn">
                            <a href="<?= get_permalink() ?>" class="default"><?= $langTxt['Lire'] ?> </a>
                            <span class="_floatRight share">

                        <span><?= $langTxt['Partager'] ?> : </span>
                           <a href="http://www.linkedin.com/shareArticle?mini=true&url=<?= get_permalink() ?>"
                              target="_blank"><i class="fa fa-linkedin"></i></a>
                        <a href="http://www.facebook.com/sharer.php?u=<?= get_permalink() ?>" target="_blank"><i
                                    class="fa fa-facebook"></i></a>
                        <a href="https://plus.google.com/share?url=<?= get_permalink() ?>" target="_blank"><i
                                    class="fa fa-google-plus"></i></a>

                    </span>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <div class="paginationBlog" style="    height: 71px;">

                <?php
                the_posts_pagination(array(
                    'mid_size' => 2,
                    'prev_text' => __('<<', 'textdomain'),
                    'next_text' => __('>>', 'textdomain'),
                    'current'=>$paged
                ));
                ?>
            </div>
        </div>
    </div>
    <div class="sidebarPost ">


        <div class="blogPage">
            <div class="sidebar">
                <div class="search">
                    <form id="searchSideBar" method="get" action="<?php echo home_url(); ?>" role="search">
                        <input class="input _full-width" type="text" name="s" placeholder="<?= $langTxt['recherche'] ?>"
                               value="<?php echo get_search_query() ?>">
                    </form>
                    <i id="searchBtnSideBar" class="fa fa-search"></i>
                </div>
                <div class="social">
                    <span><?= $langTxt['suivez nous sur'] ?>: </span>
                    <ul class="topnav socialLinks">
                        <li><a target="_blank" href="https://www.linkedin.com/company/27093149/"><i
                                        class="fa fa-linkedin"></i></a></li>
                        <li><a target="_blank" href="https://www.facebook.com/groupedentaireapi"><i
                                        class="fa fa-facebook"></i></a></li>
                        <li><a target="_blank" href="https://plus.google.com/+Apigroupe?hl=fr"><i
                                        class="fa fa-google-plus"></i></a></li>
                        <li><a target="_blank" href="https://www.youtube.com/channel/UCsil0debGF6WR0EzE7tGaYg/videos"><i
                                        class="fa fa-youtube-play"></i></a></li>
                    </ul>
                </div>
                <div class="titleBlock"><?= $langTxt['Categories'] ?></div>
                <?php
                $categories = get_categories(array(
                    'orderby' => 'count',
                    'order' => 'DESC',
                    "exclude" => [1, 124]
                ));
                $limit = 8;
                $i = 0;
                foreach ($categories as $category) {
                    $i++;
                    if ($limit < $i)
                        break;
                    echo "<a href='" . get_category_link($category->term_id) . "'>" . $category->name . " (" . $category->count . ")</a>";
                }
                ?>
                <div class="titleBlock">
                    <?= $langTxt['CONSEILS LES PLUS RÉCENTES'] ?>
                </div>

                <?php
                $wp_query = new WP_Query([
                    'post_type' => "post",
                    "post_status" => "publish",
                    'posts_per_page' => 3,
                    'paged' => 1,
                    "order" => "DESC"
                ]);
                while ($wp_query->have_posts()) {
                    $wp_query->the_post();
                    echo "<a href='" . get_permalink() . "'>" . get_the_title() . "<div class='date'> " . $langTxt['Publié'] . " " . get_the_date('d') . " " . get_the_date('F') . " " . get_the_date('Y') . "</div></a>";
                }
                ?>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>
