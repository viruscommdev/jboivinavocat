<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
get_header();
?>

    <style>
        @media (min-width: 426px) {
            .cover {
                height: 23.5vw !important;
            }
        }
    </style>
    <div class="cover">

        <div class="desktop">
            <img width="1997" height="481"
                 src="/wp-content/uploads/2017/10/Api_siteweb_Nos-Conseils_01.jpg"
                 class="attachment-full size-full" alt="Conseils API"></div>
        <div class="mobile">

            <img width="320" height="623" src="/wp-content/uploads/2017/10/header_zone-Nos-Conseils.jpg"
                 class="attachment-full size-full" alt="">
            <div class="logo_cover_bar"
                 style="background-image: url('/wp-content/themes/Skeleton-Theme/img/x_mobile_02.png')"></div>
        </div>
    </div>
    <div class="content20vw flexDisplay center">
        <div class="container">
            <h1 style="text-align: center">Oups ! Erreur 404</h1>
            <p style="text-align: center">Nous sommes désolés, la page à laquelle vous essayez d'accéder n'existe pas ou a
                changé d'emplacement.</p>
            <p style="text-align: center">Visitez la <a href="/">page d’accueil</a> du site</p>
        </div>

    </div>
<?php get_footer();