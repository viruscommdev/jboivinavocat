<?php
get_header();
the_post();
?>
<style>
    @media (min-width: 426px) {
        .cover {
            height: 23.5vw !important;
        }
    }
</style>
<div class="cover">

    <div class="desktop">
        <img width="1997" height="481"
             src="/wp-content/uploads/2017/10/Api_siteweb_Nos-Conseils_01.jpg"
             class="attachment-full size-full" alt="Conseils API"></div>
    <div class="mobile">

        <img width="320" height="623" src="/wp-content/uploads/2017/10/header_zone-Nos-Conseils.jpg"
             class="attachment-full size-full" alt="">
        <div class="logo_cover_bar"
             style="background-image: url('/wp-content/themes/Skeleton-Theme/img/x_mobile_02.png')"></div>
    </div>
</div>
<div class="postPage">
    <div class="sidePost">
        <div class="titleblock">
            <h1 class="title font32 fontextrabold allUp">
                <?= get_the_title() ?>
            </h1>

            <div class="btn">
                <span class="share">
                        <span><?=$langTxt['Partager']?> : </span>
                         <a href="http://www.linkedin.com/shareArticle?mini=true&url=<?= get_permalink() ?>" target="_blank"><i class="fa fa-linkedin"></i></a>
                        <a href="http://www.facebook.com/sharer.php?u=<?= get_permalink() ?>" target="_blank"><i class="fa fa-facebook"></i></a>
                        <a href="https://plus.google.com/share?url=<?= get_permalink() ?>" target="_blank"><i class="fa fa-google-plus"></i></a>

                    </span>
            </div>
        </div>
        <div class="belowTitle blogPage">
            <div class="date colorblue"><?=$langTxt['Publié']?> <?= get_the_date('d/m/Y') ?></div>

        </div>
        <div class="blogPage">
            <div class="articlebig">
                <div class="img">
                    <?= get_the_post_thumbnail(null, "full") ?>
                    <div class="bg">
                    </div>
                </div>
            </div>
        </div>
        <div class="contentPost">
            <?php
            the_content();
            ?>
        </div>
        <?php comments_template(); ?>
    </div>
    <div class="sidebarPost">

        <div class="blogPage">
            <div class="sidebar">
                <div class="search">
                    <form id="searchSideBar" method="get" action="<?php echo home_url(); ?>" role="search">
                        <input class="input _full-width" type="text" name="s" placeholder="<?= $langTxt['recherche'] ?>"
                               value="<?php echo get_search_query() ?>">
                    </form>
                    <i id="searchBtnSideBar" class="fa fa-search"></i>
                </div>
                <div class="social">
                    <span><?= $langTxt['suivez nous sur'] ?>: </span>
                    <ul class="topnav socialLinks">
                        <li><a target="_blank"  href="https://www.linkedin.com/company/27093149/"><i class="fa fa-linkedin"></i></a></li>
                        <li><a target="_blank" href="https://www.facebook.com/groupedentaireapi"><i class="fa fa-facebook"></i></a></li>
                        <li><a target="_blank"   href="https://plus.google.com/+Apigroupe?hl=fr"><i class="fa fa-google-plus"></i></a></li>
                        <li><a target="_blank"  href="https://www.youtube.com/channel/UCsil0debGF6WR0EzE7tGaYg/videos"><i class="fa fa-youtube-play"></i></a></li>
                    </ul>
                </div>
                <div class="titleBlock"><?= $langTxt['Categories'] ?></div>
                <?php
                $categories = get_categories( array(
                    'orderby' => 'count',
                    'order'   => 'DESC',
                    "exclude"=>[1,124]
                ) );
                $limit=8;$i=0;
                foreach ($categories as $category){
                    $i++;
                    if($limit<$i)
                        break;
                    echo "<a href='".get_category_link( $category->term_id )."'>".$category->name." (".$category->count.")</a>";
                }
                ?>
                <div class="titleBlock">
                    <?= $langTxt['CONSEILS LES PLUS RÉCENTES'] ?>
                </div>

                <?php
                $wp_query=new WP_Query( [
                    'post_type'=>"post",
                    "post_status"=>"publish",
                    'posts_per_page' => 3,
                    'paged'          => 1,
                    "order"=>"DESC"
                ]);
                while($wp_query->have_posts())
                {
                    $wp_query->the_post();
                    echo "<a href='".get_permalink()."'>".get_the_title()."<div class='date'> ".$langTxt['Publié']." ".get_the_date('d')." ".get_the_date('F')." ".get_the_date('Y')."</div></a>";
                }
                ?>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>
