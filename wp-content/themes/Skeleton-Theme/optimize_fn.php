<?php

/**
 * Created by PhpStorm.
 * User: mohamed
 * Date: 17/11/2017
 * Time: 11:22
 */
class optimize_fn
{
    function __construct()
    {
        if(!is_admin()){
            $this->noWPMLCSS();
//            $this->filter_css_to_cssLoad();
//            $this->filter_js_add_tag();
//            $this->filter_bwp_minify();
//            $this->preload_css();
        }
    }
    function preload_css(){
        add_action("wp_head",function(){
            ?>
            <style>
                <?php include_once("light_css.css"); ?>
            </style>
            <?php
        });
    }
    function noWPMLCSS(){
        define("ICL_DONT_LOAD_LANGUAGE_SELECTOR_CSS",true);
    }
    function addLoadCSSScript()
    {
        //https://github.com/filamentgroup/loadCSS/blob/master/src/loadCSS.js
        ?>
        <script>
            !function(e){"use strict";var n=function(n,t,o){function i(e){return a.body?e():void setTimeout(function(){i(e)})}function r(){l.addEventListener&&l.removeEventListener("load",r),l.media=o||"all"}var d,a=e.document,l=a.createElement("link");if(t)d=t;else{var s=(a.body||a.getElementsByTagName("head")[0]).childNodes;d=s[s.length-1]}var f=a.styleSheets;l.rel="stylesheet",l.href=n,l.media="only x",i(function(){d.parentNode.insertBefore(l,t?d:d.nextSibling)});var u=function(e){for(var n=l.href,t=f.length;t--;)if(f[t].href===n)return e();setTimeout(function(){u(e)})};return l.addEventListener&&l.addEventListener("load",r),l.onloadcssdefined=u,u(r),l};"undefined"!=typeof exports?exports.loadCSS=n:e.loadCSS=n}("undefined"!=typeof global?global:this);
        </script>
        <?php
    }
    public $jsAddTag="defer";
    function filter_bwp_minify(){

        add_action("wp_head",array(&$this,"addLoadCSSScript"),1);
        add_filter("bwp_minify_get_tag",function($return, $string, $type, $group){
            if($type=="script"){
                return "<script ".$this->jsAddTag." type='text/javascript' src='".$string."'></script>";
            }
            if($type=="style"){
                return '<script>loadCSS( "'.$string.'" );</script>';
            }
        },1,4);
    }
    function filter_css_to_cssLoad()
    {
        add_action("wp_head",array(&$this,"addLoadCSSScript"),1);
        add_filter('style_loader_tag', function ($html, $handle, $href, $media) {
            return '<script>loadCSS( "'.$href.'" );</script>';
        }, 10, 4);
    }
    function filter_js_add_tag(){
        add_filter( 'script_loader_tag', function( $tag, $handle, $src){
            return "<script ".$this->jsAddTag." type='text/javascript' src='".$src."'></script>";
        }, 10, 3 );
    }
}

new optimize_fn();