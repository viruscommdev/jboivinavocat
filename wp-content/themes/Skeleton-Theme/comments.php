<?php
/**
 * The template for displaying Comments.
 *
 * The area of the page that contains both current comments
 * and the comment form. The actual display of comments is
 * handled by a callback to shape_comment() which is
 * located in the inc/template-tags.php file.
 *
 * @package Shape
 * @since Shape 1.0
 */
?>

<?php
/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if (post_password_required())
    return;
?>

<div id="comments" class="comments-area">
    <?php

    $commenter = wp_get_current_commenter();
    $req = get_option('require_name_email');
    $aria_req = ($req ? " aria-required='true'" : '');
    $html_req = ($req ? " required='required'" : '');
    $html5 = true;
    comment_form(["fields" =>
        array(
            'author' => '<p class="comment-form-author">' .
                '<input id="author" name="author" placeholder="' . __('Name') . '" type="text" value="' . esc_attr($commenter['comment_author']) . '" size="30" maxlength="245"' . $aria_req . $html_req . ' /></p>',
            'email' => '<p class="comment-form-email">' .
                '<input id="email" name="email" placeholder="' . __('Email') . '" ' . ($html5 ? 'type="email"' : 'type="text"') . ' value="' . esc_attr($commenter['comment_author_email']) . '" size="30" maxlength="100" aria-describedby="email-notes"' . $aria_req . $html_req . ' /></p>'
        ),

        'comment_field' => '<p class="comment-form-comment"> <textarea placeholder="'. _x('Comment', 'noun') .'" id="comment" name="comment" cols="45" rows="8" maxlength="65525" aria-required="true" required="required"></textarea></p>']); ?>

    <?php // You can start editing here -- including this comment! ?>

    <?php if (have_comments()) : ?>
        <div class="titleCO"> Commentaires</div>
        <?php if (get_comment_pages_count() > 1 && get_option('page_comments')) : // are there comments to navigate through? If so, show navigation ?>
            <nav role="navigation" id="comment-nav-above" class="site-navigation comment-navigation">
                <h1 class="assistive-text"><?php _e('Comment navigation', 'shape'); ?></h1>
                <div class="nav-previous"><?php previous_comments_link(__('&larr; Older Comments', 'shape')); ?></div>
                <div class="nav-next"><?php next_comments_link(__('Newer Comments &rarr;', 'shape')); ?></div>
            </nav><!-- #comment-nav-before .site-navigation .comment-navigation -->
        <?php endif; // check for comment navigation ?>
        <ol class="commentlist">
            <?php
            /* Loop through and list the comments. Tell wp_list_comments()
             * to use shape_comment() to format the comments.
             * If you want to overload this in a child theme then you can
             * define shape_comment() and that will be used instead.
             * See shape_comment() in inc/template-tags.php for more.
             */
            wp_list_comments(array('callback' => 'shape_comment'));
            ?>
        </ol><!-- .commentlist -->

        <?php if (get_comment_pages_count() > 1 && get_option('page_comments')) : // are there comments to navigate through? If so, show navigation ?>
            <nav role="navigation" id="comment-nav-below" class="site-navigation comment-navigation">
                <h1 class="assistive-text"><?php _e('Comment navigation', 'shape'); ?></h1>
                <div class="nav-previous"><?php previous_comments_link(__('&larr; Older Comments', 'shape')); ?></div>
                <div class="nav-next"><?php next_comments_link(__('Newer Comments &rarr;', 'shape')); ?></div>
            </nav><!-- #comment-nav-below .site-navigation .comment-navigation -->
        <?php endif; // check for comment navigation ?>

    <?php endif; // have_comments() ?>

    <?php
    // If comments are closed and there are comments, let's leave a little note, shall we?
    if (!comments_open() && '0' != get_comments_number() && post_type_supports(get_post_type(), 'comments')) :
        ?>
        <p class="nocomments"><?php _e('Comments are closed.', 'shape'); ?></p>
    <?php endif; ?>



</div><!-- #comments .comments-area -->