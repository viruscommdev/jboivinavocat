jQuery(window).load(function () {
    $ = jQuery;
    jQuery(".coverWindow").removeClass("coverUp");
    jQuery(".holdingImage").addClass("removeLogo");
    jQuery("body").removeClass("overflowHiddenTemp");
    setTimeout(function () {
        jQuery(".coverWindow").hide();
    }, 800);
    jQuery(".galleryClinique .gallery .image img").click(function () {
        var modal = document.getElementById('modalImage');
        var modalImg = document.getElementById("imagePopup");
        var captionText = document.getElementById("imageCaption");
        modal.style.display = "flex";
        modalImg.src = $(this).attr("src");
        captionText.innerHTML = $(this).attr("alt");
    });
    jQuery(".galleryClinique .gallery .image .plus").click(function () {
        var img = $(this).parents(".image:first").find("img").first();
        var modal = document.getElementById('modalImage');
        var modalImg = document.getElementById("imagePopup");
        var captionText = document.getElementById("imageCaption");
        modal.style.display = "flex";
        modalImg.src = $(img).attr("src");
        captionText.innerHTML = $(img).attr("alt");
    });
    jQuery("#imgclose").click(function () {
        var modal = document.getElementById('modalImage');
        modal.style.display = "none";
    });
    jQuery(".modeimg .fa-angle-right").click(function () {
        var list = [];
        jQuery(".gallery img").each(function () {
            list.push(jQuery(this).attr("src"));
        });
        index = 0;
        for (i in list) {
            if (jQuery("#imagePopup").attr("src") == list[i])
                index = i;
        }
        if (index < list.length) {
            jQuery("#imagePopup").attr("src", list[Number(index) + 1]);
        }
    });
    jQuery(".modeimg .fa-angle-left").click(function () {
        var list = [];
        jQuery(".gallery img").each(function () {
            list.push(jQuery(this).attr("src"));
        });
        index = 0;
        for (i in list) {
            if (jQuery("#imagePopup").attr("src") == list[i])
                index = i;
        }
        if (Number(index) != 0) {
            jQuery("#imagePopup").attr("src", list[Number(index) - 1]);
        }
    });
    $(".accordion").accordion();
    jQuery("input[name='selectfaq']").change(function () {
        console.log($(this).attr("id"));

        $(".accordion").hide();
        if ($(this).attr("id") == "checkbox-1") {
            $(".accordion").show();
        }
        else {
            $(".accordion[data-id='" + $(this).attr("id") + "']").show();
        }
    });
    $("#cssmenu  .button.main").click( function () {
        var mainmenu = $(this).next('ul');
        if (mainmenu.hasClass('open')) {
            $("#cssmenu > .logo").removeClass("hide");
            $(".headerMobile").addClass("open");
            $("#cssmenu > .sidebarMenuIcon").show();
        }
        else {
            $("#cssmenu > .logo").addClass("hide");
            $(".headerMobile").removeClass("open");

            $("#cssmenu > .sidebarMenuIcon").hide();
        }
    });
    $(".equipeMascouche").click(function () {
        $(".member").hide();
        $(".member.Mascouche").show();
    });
    $(".equipeLaval").click(function () {
        $(".member").hide();
        $(".member.Laval").show();
    });
    $(".page-child a.default").each(function () {
       if($(this).css("position")=="relative" && $(this).parents(".icons:first").length==0)
           $(this).addClass("relative");
    });
    $("#searchBtn").click(function () {
       $("#formHeader").submit();
    });
    $("#searchBtnSideBar").click(function () {
       $("#searchSideBar").submit();
    });
    $(".sidebarMenuIcon a").click(function () {
        $("#cssmenu  .button.main").click();
    });
    $(".form select").change(function () {
       if($(this).val()=="selected")
           $(this).removeClass("colorBlack");
       else
           $(this).addClass("colorBlack");
    });
    if(jQuery(".page-id-576,.page-id-5359").length>0){
        jQuery('#btn-map').click(function(){
            var adr=jQuery('#adresse').val();
            var des=jQuery('#adress-des').val();
            var src='';
            if(des=='Laval')
            {
                src="https://www.google.com/maps/dir/"+adr+" /45.6099957,-73.71978790000003";

            }
            else
            {
                src="https://www.google.com/maps/dir/"+adr+" /45.7276576,-73.62051600000001";
            }
            window.open(src);
        });

    }
    if(jQuery(".postPage").length>0){
        jQuery(".postPage *").each(function(){
           if($(this).html()=="&nbsp;")
               $(this).remove();
        });
    }
    if($("html").attr("lang")=="en-US")
    {
        var contactLink="/en/who-are-we/";
        var dentiste_laval="/en/dental-clinic-laval/";

        jQuery(".footer .rightFooterMenu .menu-item-5262 > a").attr("href",contactLink);
        jQuery(".footer .rightFooterMenu .menu-item-5263 > a").attr("href",dentiste_laval);
    }else{

        var contactLink="/qui-sommes-nous/";
        var dentiste_laval="/dentiste-laval/";
        jQuery(".footer .rightFooterMenu .menu-item-10 > a").attr("href",contactLink);
        jQuery(".footer .rightFooterMenu .menu-item-145 > a").attr("href",dentiste_laval);
    }
});

function cssUpdate(selecter) {
    if ($(selecter).length == 0) {
        console.log("element not found");
        return false;
    }
    else
        console.log("reloading..");
    var link = $(selecter).attr("href");
    $(selecter).remove();
    $('head').append('<link rel="stylesheet" id="' + selecter.replace("#", "") + '" type="text/css" href="' + link + '">');

}