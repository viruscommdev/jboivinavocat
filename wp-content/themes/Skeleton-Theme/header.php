<!DOCTYPE html>
<html <?php global $langTxt; language_attributes(); ?>>
<head>
    <title><?php wp_title(); ?></title>
    <meta charset="<?php bloginfo('charset'); ?>"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>"/>
    <?php wp_head(); ?>
    <style type="text/css" media="screen">
        @media screen and ( max-width: 782px ) {
            html { margin-top: 0px !important; }
            * html body { margin-top: 0px !important; }
        }
    </style>
</head>

<body <?php body_class("overflowHiddenTemp"); ?>>
<?php if ( function_exists( 'gtm4wp_the_gtm_tag' ) ) { gtm4wp_the_gtm_tag(); } ?>
<div class="navbarPos">
    <a class="logo" href="<?=$langTxt['linkHome']?>">
        <?= wp_get_attachment_image("37", "full") ?>
    </a>
    <?php $startMenuWalker = new Menu_Walker_Nav_Menu();
    wp_nav_menu(array('theme_location' => 'main_menu', 'menu_class' => 'topnav _floatRight', 'depth' => 3, 'container' => false, 'walker' => $startMenuWalker)); ?>
    <div class="menuTop _floatRight">
        <ul class="topnav _floatRight">
            <li><a target="_blank" href="https://www.linkedin.com/company/27093149/"><i class="fa fa-linkedin"></i></a></li>
            <li><a target="_blank"  href="https://www.facebook.com/groupedentaireapi"><i class="fa fa-facebook"></i></a></li>
            <li><a target="_blank"  href="https://plus.google.com/+Apigroupe?hl=fr"><i class="fa fa-google-plus"></i></a></li>
            <li  style="margin-right: 30px;"><a target="_blank"  href="https://www.youtube.com/channel/UCsil0debGF6WR0EzE7tGaYg/videos"><i class="fa fa-youtube-play"></i></a></li>
             <li>Laval <a href="tel:4506226060" class="tel">450 662-6060</a></li>
            <li style="    margin: 5px 13px 0px 5px;">|</li>
            <li>Mascouche <a href="tel:4509669888" class="tel">450 966-9888</a></li>
            <li class="searchMenu"><a href="#">
                    <form id="formHeader" method="get" action="<?php echo home_url(); ?>" role="search">
                        <input class="inputSearch" type="text" name="s" style="margin-bottom: 0px!important;"
                               placeholder="<?=$langTxt['recherche']?>" value="<?php echo get_search_query() ?>">
                    </form>
                    <i id="searchBtn" class="fa fa-search tel"></i></a></li>
            <li>
                <?php
                wp_nav_menu(array('theme_location' => 'lang_menu','container' => false,"menu_class"=>"langMenu")); ?>
            </li>
        </ul>
    </div>
</div>