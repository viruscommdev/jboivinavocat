<?php

/**
 * Created by PhpStorm.
 * User: mohamed
 * Date: 03/10/2017
 * Time: 15:06
 */
class init_theme
{
    function __construct()
    {
        add_theme_support('post-thumbnails');
        add_theme_support('automatic-feed-links');
        add_theme_support('title-tag');

        add_action('after_setup_theme', array(&$this, 'add_class_menu'));
        add_action('init', array(&$this, 'menu_setup'));

        add_action('wp_enqueue_scripts', array(&$this, 'add_script'), 1);
        add_action('wp_footer', array(&$this, 'remove_action'));

        remove_action('wp_head', 'print_emoji_detection_script', 7);
        remove_action('wp_print_styles', 'print_emoji_styles');
        $this->resgister_for_theme();
        //remove srcset
        add_filter('wp_calculate_image_srcset_meta', '__return_empty_array');
    }

    public function resgister_for_theme()
    {
        if (function_exists('register_nav_menus')) {
            register_nav_menus(
                array(
                    'main_menu' => 'Main Menu',
                    "lang_menu" => 'Language menu'
                )
            );
        }

        add_image_size( 'custom-med-500', 500, 300,true );
        add_image_size( 'custom-med-461', 461, 307,true );
    }

    public function menu_setup()
    {
        register_nav_menu('top-bar', 'Bootstrap Top Menu');
    }

    public function add_class_menu()
    {
        init_menu();
    }

    public function add_script()
    {
        $template_url = get_template_directory_uri();
        wp_enqueue_script("jquery");
        wp_enqueue_script("main-script", $template_url . "/js/main.js");
        wp_enqueue_script("accordion-script", $template_url . "/bower_components/woco-accordion/woco.accordion.js");


        wp_enqueue_style('framework-style', $template_url . '/bower_components/beauter/beauter.css');
        wp_enqueue_style('accordion-style', $template_url . '/bower_components/woco-accordion/woco-accordion.css');
//Main Style
        wp_enqueue_style('main-style', $template_url . '/style.css');
        //mobile menu
        wp_enqueue_style('menumobile-style', $template_url . '/outside_components/mobileMenu/style.css');
        wp_enqueue_script("menumobile-script", $template_url . "/outside_components/mobileMenu/index.js");
        // added style.scss
//        wp_enqueue_style('main-font', 'https://fonts.googleapis.com/css?family=Montserrat:300,400,600,700,800');
        wp_enqueue_style('icon-font', $template_url . '/bower_components/font-awesome/css/font-awesome.min.css');
    }

    public function remove_action()
    {
        wp_deregister_script('wp-embed');
        // REMOVE EMOJI ICONS

    }

}

function init_menu()
{
    class Menu_Walker_Nav_Menu extends Walker_Nav_Menu
    {
        public $mobileNav = "desktop";
        private $deepID = null;

        function start_lvl(&$output, $depth = 0, $args = array())
        {

            $indent = str_repeat("\t", $depth);
            $output .= "\n$indent<ul  id=\"dropdown" . $this->mobileNav . $this->deepID . "\" class=\"dropdown-content\">\n";

        }

        function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0)
        {

            if (!is_object($args)) {
                return; // menu has not been configured
            }

            $indent = ($depth) ? str_repeat("\t", $depth) : '';
            $li_attributes = '';
            $class_names = $value = '';

            $classes = empty($item->classes) ? array() : (array)$item->classes;
            $classes[] = ($args->has_children) ? 'dropdown' : '';
            $classes[] = ($item->current || $item->current_item_ancestor) ? 'active' : '';
            $classes[] = 'menu-item-' . $item->ID;
//				$classes[] = 'hvr-underline-from-center';


            $class_names = join(' ', apply_filters('nav_menu_css_class', array_filter($classes), $item, $args));
            $class_names = ' class="' . esc_attr($class_names) . '"';

            $id = apply_filters('nav_menu_item_id', 'menu-item-' . $item->ID, $item, $args);
            $id = strlen($id) ? ' id="' . esc_attr($id) . '"' : '';

            $output .= $indent . '<li' . $id . $value . $class_names . $li_attributes . '>';

            $attributes = !empty($item->attr_title) ? ' title="' . esc_attr($item->attr_title) . '"' : '';
            $attributes .= !empty($item->target) ? ' target="' . esc_attr($item->target) . '"' : '';
            $attributes .= !empty($item->xfn) ? ' rel="' . esc_attr($item->xfn) . '"' : '';
            $attributes .= !empty($item->url) ? ' href="' . esc_attr($item->url) . '"' : '';

            $attributes .= ($args->has_children) ? ' class="dropdown-button" href="#!" data-activates="dropdown' . $this->mobileNav . $this->deepID . '"' : '';

            $item_output = $args->before;
            $item_output .= '<a' . $attributes . '>';
            $item_output .= $args->link_before . apply_filters('the_title', $item->title, $item->ID) . $args->link_after;
            $item_output .= ($args->has_children) ? ' <b class="caret"></b></a>' : '</a>';
            $item_output .= $args->after;
            $output .= apply_filters('walker_nav_menu_start_el', $item_output, $item, $depth, $args);
        }

        function display_element($element, &$children_elements, $max_depth, $depth = 0, $args, &$output)
        {

            if (!$element)
                return;

            $id_field = $this->db_fields['id'];

            //display this element
            if (is_array($args[0]))
                $args[0]['has_children'] = !empty($children_elements[$element->$id_field]);
            else if (is_object($args[0]))
                $args[0]->has_children = !empty($children_elements[$element->$id_field]);
            $cb_args = array_merge(array(&$output, $element, $depth), $args);
            $this->deepID = $element->$id_field;
            call_user_func_array(array(&$this, 'start_el'), $cb_args);

            $id = $element->$id_field;

            // descend only when the depth is right and there are childrens for this element
            if (($max_depth == 0 || $max_depth > $depth + 1) && isset($children_elements[$id])) {

                foreach ($children_elements[$id] as $child) {
                    $this->deepID = $id;
                    if (!isset($newlevel)) {
                        $newlevel = true;
                        //start the child delimiter
                        $cb_args = array_merge(array(&$output, $depth), $args);
                        call_user_func_array(array(&$this, 'start_lvl'), $cb_args);
                    }
                    $this->display_element($child, $children_elements, $max_depth, $depth + 1, $args, $output);
                }
                unset($children_elements[$id]);
            }

            if (isset($newlevel) && $newlevel) {
                //end the child delimiter
                $cb_args = array_merge(array(&$output, $depth), $args);
                call_user_func_array(array(&$this, 'end_lvl'), $cb_args);
            }

            //end this element
            $cb_args = array_merge(array(&$output, $element, $depth), $args);
            call_user_func_array(array(&$this, 'end_el'), $cb_args);
        }
    }
}

class intern_fn
{
    function __construct()
    {
        add_filter('get_the_excerpt', array(&$this, "get_excerpt"));
    }
    public function get_excerpt($excerpt = "", $limit = 140)
    {

        $excerpt = preg_replace(" (\[.*?\])", '', $excerpt);
        $excerpt = strip_shortcodes($excerpt);
        $excerpt = strip_tags($excerpt);
        $excerpt = substr($excerpt, 0, $limit);
        $excerpt = substr($excerpt, 0, strripos($excerpt, " "));
        $excerpt = trim(preg_replace('/\s+/', ' ', $excerpt));
        $excerpt = $excerpt . '...';
        return $excerpt;
    }
}

$intern_fn = new intern_fn();
new init_theme();
include_once "content/lang.php";
//include_once "integrated_vc.php";
include_once "content/template.php";
include_once "optimize_fn.php";