<?php
/**
 * Created by PhpStorm.
 * User: mohamed
 * Date: 08/11/16
 * Time: 13:22
 */
include "settings.php";
// --no-data   for table schema only
exec("mysqldump -u ".DB_USER." -p".DB_PASSWORD." --extended-insert=FALSE ".DB_NAME." > db.php");
//replace new utf8mb4_unicode_520_ci to utf8 --pretty match doesn't make different :p yet some old mysql doesn't allow new one
$result=str_replace(["utf8mb4_unicode_520_ci","utf8mb4"],["utf8_unicode_ci","utf8"],file_get_contents("db.php"));
file_put_contents("db.php","/*<?php  die('nothing to see here'); ?>*/\n".$result);