<?php
/**
 * Created by PhpStorm.
 * User: mohamed
 * Date: 23/11/16
 * Time: 10:32
 */
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
include "settings.php";
// Create connection
$conn = new PDO("mysql:host=".DB_HOST.";dbname=".DB_NAME, DB_USER, DB_PASSWORD);
$command = "mysql -u ".DB_USER." -p".DB_PASSWORD." -h ".DB_HOST." -D ".DB_NAME." < ./db.php";
if(!isset($_GET['noupdate']))
$output = shell_exec($command);
else{
    echo $command;
}
$actual_link = "http://".$_SERVER['HTTP_HOST']."/";
$stmt = $conn->prepare("update wp_options set option_value='$actual_link' where option_name='siteurl' or option_name='home'");
$stmt->execute();
